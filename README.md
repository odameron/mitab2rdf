# mitab2rdf

Convert a [MITAB (PSI-MI TAB v2.5)](https://code.google.com/archive/p/psimi/wikis/PsimiTabFormat.wiki) protein interactions file into RDF

# Todo
- [ ] generate data in a named graph
- [ ] generate metadata as VoID
