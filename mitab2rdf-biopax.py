#! /usr/bin/env python3

import argparse
import csv

#taxon = '559292' # Saccharomyces cerevisiae S288C

def getInteractorsUniprotID(mitabLine):
	interactorA = mitabLine[0].replace('entrez gene/locuslink:', 'entrezGene:')
	interactorB = mitabLine[1].replace('entrez gene/locuslink:', 'entrezGene:')
	if mitabLine[0].startswith('uniprotkb:'):
		interactorA = mitabLine[0]
	else:
		for altIdent in mitabLine[2].split('|'):
			if altIdent.startswith('uniprotkb:'):
				interactorA = altIdent
				break
			elif altIdent.startswith('uniprot/swiss-prot:'):
				interactorA = 'uniprotkb:' + altIdent[19:]
				break
	if mitabLine[1].startswith('uniprotkb:'):
		interactorA = mitabLine[1]
	else:
		for altIdent in mitabLine[3].split('|'):
			if altIdent.startswith('uniprotkb:'):
				interactorB = altIdent
				break
			elif altIdent.startswith('uniprot/swiss-prot:'):
				interactorB = 'uniprotkb:' + altIdent[19:]
				break
	return (interactorA, interactorB)


def getInteractorLabels(aliasesList):
	labels = []
	for currentAlias in aliasesList:
		if currentAlias.startswith('uniprotkb:'):
			labels.append(currentAlias.replace('uniprotkb:', '').replace('(gene_name)', '').replace('"', ''))
		elif currentAlias.startswith('entrez gene/locuslink:'):
			labels.append(currentAlias.replace('entrez gene/locuslink:', '').replace('(gene name synonym)', '').replace('"', ''))
	return labels


def getInteractorTaxids(taxidList):
	taxids = []
	taxidLabels = []
	for currentTaxid in taxidList:
		parenthesisIndex = currentTaxid.find('(')
		if parenthesisIndex == -1:
			taxids.append(currentTaxid)
			taxidLabels.append("")
		else:
			taxids.append(currentTaxid[:parenthesisIndex])
			taxidLabels.append(currentTaxid[parenthesisIndex+1:-1])
	return (taxids, taxidLabels)


def getInteractionTypes(interactionTypesList):
	interactionTypes = []
	interactionTypeLabels = []
	for currentIT in interactionTypesList:
		if currentIT.startswith('psi-mi:"MI:'):	# BioGRID abuses the mitab syntax with multiple layers of identifiers (e.g. psi-mi:"MI:0915"(physical association))
			parenthesisIndex = currentIT.find('(')
			if parenthesisIndex == -1:
				interactionTypes.append('mi:' + currentIT[11:-1])
				interactionTypeLabels.append("")
			else:
				interactionTypes.append('mi:' + currentIT[11:parenthesisIndex-1])
				interactionTypeLabels.append(currentIT[parenthesisIndex+1:currentIT.find(')')])
	return (interactionTypes, interactionTypeLabels)


def parsePSIMI(filepath, taxon, database):
	with open(filepath.replace('.mitab', '.ttl'), 'w') as ttlfile:
		#ttlfile.write()
		ttlfile.write('@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.\n')
		ttlfile.write('@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>.\n')
		ttlfile.write('@prefix owl: <http://www.w3.org/2002/07/owl#>.\n')
		ttlfile.write('@prefix xsd: <http://www.w3.org/2001/XMLSchema#>.\n')
		ttlfile.write('@prefix skos: <http://www.w3.org/2004/02/skos/core#>.\n')
		ttlfile.write('@prefix dcterms: <http://purl.org/dc/terms/> .\n')
		ttlfile.write('\n')
		ttlfile.write('@prefix psimi: <http://www.psidev.info/>.\n')
		ttlfile.write('@prefix mi: <http://purl.obolibrary.org/obo/MI_>.\n')
		ttlfile.write('@prefix pubmed: <https://www.ncbi.nlm.nih.gov/pubmed/>.\n')
		ttlfile.write('@prefix taxid: <http://purl.uniprot.org/taxonomy/>.\n')
		ttlfile.write('@prefix up: <http://purl.uniprot.org/core/>.\n')
		ttlfile.write('@prefix uniprotkb: <http://purl.uniprot.org/uniprot/>.\n')
		ttlfile.write('@prefix entrezGene: <https://www.ncbi.nlm.nih.gov/gene/>.\n')
		ttlfile.write('@prefix wp: <http://vocabularies.wikipathways.org/wp#>.\n')
		ttlfile.write('@prefix bp3: <http://www.biopax.org/release/biopax-level3.owl#>.\n')
		ttlfile.write('\n')
		ttlfile.write('@prefix apid: <https://apid.dep.usal.es/>.\n')
		ttlfile.write('@prefix biogrid: <http://thebiogrid.org/>.\n')
		ttlfile.write('@prefix dip: <http://dip.doe-mbi.ucla.edu/>.\n')
		ttlfile.write('@prefix intact: <https://www.ebi.ac.uk/intact/interaction/>.\n')

		##ttlfile.write('@prefix : <>.')
		#ttlfile.write('\n')
		#ttlfile.write('apid:interactor1 rdfs:subPropertyOf apid:interactor .\n')
		#ttlfile.write('apid:interactor2 rdfs:subPropertyOf apid:interactor .\n')
		#ttlfile.write('apid:interactor1 rdfs:label "has interactor" .\n')
		#ttlfile.write('apid:interactor2 rdfs:label "has interactor" .\n')
		ttlfile.write('\n')


		with open(filepath) as csvfile:
			psimireader = csv.reader(csvfile, delimiter='\t')

			lineNumber = 0
			for row in psimireader:
				if lineNumber == 0:
					for i in range(len(row)):
						print(str(i) + '\t' + row[i])
					print('\n')
					lineNumber += 1
					continue

				#print('\n' + str(row))
				# interaction identifiers contain a reference to the taxon for avoiding conflicts
				currentIdent = 'apid:apid-taxon{}-interaction{}'.format(taxon, lineNumber)
				currentIdent = '{}:{}-taxon{}-interaction{}'.format(database, database, taxon, lineNumber)
				print(currentIdent)

				# BioPAX level 3 specifications, page 35
				ttlfile.write(currentIdent + ' rdf:type bp3:MolecularInteraction .\n') 

				(interactorA, interactorB) = getInteractorsUniprotID(row)
				ttlfile.write(currentIdent + ' bp3:participant ' + interactorA + ' .\n')
				ttlfile.write(currentIdent + ' bp3:participant ' + interactorB + ' .\n')

				ttlfile.write(interactorA + ' rdf:type up:Protein .\n')
				for interactorLabel in getInteractorLabels(row[4].split('|')):
					ttlfile.write(interactorA + ' rdfs:label "' + interactorLabel + '" .\n')
				#ttlfile.write(interactorA + ' rdfs:label "' + row[4].replace('uniprotkb:', '').replace('(gene_name)', '').replace('"', '') + '" .\n')
				(taxids, taxidLabels) = getInteractorTaxids(row[9].split('|'))
				for i in range(len(taxids)):
					interactorTaxid = taxids[i]
					interactorTaxidLabel = taxidLabels[i]
					ttlfile.write(interactorA + ' up:organism ' + taxids[i] + ' .\n')
					if taxidLabels[i] != "":
						ttlfile.write(taxids[i] + ' rdfs:label "' + taxidLabels[i] + '" .\n')
				#ttlfile.write(interactorA + ' up:organism ' + row[9][:row[9].index('(')] + ' .\n')
				#ttlfile.write(row[9][:row[9].index('(')] + ' rdfs:label "' + row[9][row[9].index('(')+1:-1] + '" .\n')

				ttlfile.write(interactorB + ' rdf:type up:Protein .\n')
				for interactorLabel in getInteractorLabels(row[5].split('|')):
					ttlfile.write(interactorB + ' rdfs:label "' + interactorLabel + '" .\n')
				#ttlfile.write(interactorB + ' rdfs:label "' + row[5].replace('uniprotkb:', '').replace('(gene_name)', '').replace('"', '') + '" .\n')
				(taxids, taxidLabels) = getInteractorTaxids(row[10].split('|'))
				for i in range(len(taxids)):
					interactorTaxid = taxids[i]
					interactorTaxidLabel = taxidLabels[i]
					ttlfile.write(interactorB + ' up:organism ' + taxids[i] + ' .\n')
					if taxidLabels[i] != "":
						ttlfile.write(taxids[i] + ' rdfs:label "' + taxidLabels[i] + '" .\n')
				#ttlfile.write(interactorB + ' up:organism ' + row[10][:row[10].index('(')] + ' .\n')
				#ttlfile.write(row[10][:row[10].index('(')] + ' rdfs:label "' + row[10][row[10].index('(')+1:-1] + '" .\n')

				ttlfile.write(currentIdent + ' psimi:detectionMethod mi:' + row[6][11:15] + ' .\n')
				ttlfile.write('mi:' + row[6][11:15] + ' rdfs:label "' + row[6][17:-1] + '" .\n')

				if row[11] != "" and row[11] != "-":
					(interactionTypes, interactionTypeLabels) = getInteractionTypes(row[11].split('|'))
					for i in range(len(interactionTypes)):
						ttlfile.write(currentIdent + ' psimi:interactionType ' + interactionTypes[i] + ' .\n')
						if interactionTypeLabels[i] != "":
							ttlfile.write(interactionTypes[i] + ' rdfs:label "' + interactionTypeLabels[i] + '" .\n')
					#for (currentInteractionType, currentInteractionTypeLabel) in getInteractionTypes(row[11].split('|')):
					#	ttlfile.write(currentIdent + ' psimi:interactionType ' + currentInteractionType + ' .\n')

				listPubID = row[8].split('|')
				listPubAuth = row[7].split('|')
				for pubIDindex in range(len(listPubID)):
					pubID = listPubID[pubIDindex]
					ttlfile.write(currentIdent + ' psimi:publicationIdentifier ' + pubID + ' .\n')
					ttlfile.write(currentIdent + ' bp3:xref ' + pubID + ' .\n')
					ttlfile.write(pubID + ' rdf:type wp:PublicationReference .\n')
					ttlfile.write(pubID + ' rdf:type bp3:PublicationXref .\n')
					if pubID.startswith('pubmed:'):
						ttlfile.write(pubID + ' bp3:db mi:0445 .\n')
						ttlfile.write('mi:0445 rdfs:label "PubMed" .\n')
						ttlfile.write(pubID + ' bp3:id "' + pubID.replace('pubmed:', '') + '" .\n')
					if pubIDindex < len(listPubAuth):
						ttlfile.write(pubID + ' psimi:publication1stAuthor "' + listPubAuth[pubIDindex] + '" .\n')
						pubAuthor = listPubAuth[pubIDindex]
						etAlIndex = pubAuthor.find(' et al.')
						if etAlIndex == -1:
							pubAuthorName = pubAuthor
						else:
							pubAuthorName = pubAuthor[:etAlIndex]
						if pubAuthorName != '':
							ttlfile.write(pubID + ' bp3:author "' + pubAuthorName + '" .\n')
						yearIndex = pubAuthor.find('(')
						if yearIndex != -1:
							pubYear = pubAuthor[pubAuthor.index('(')+1:pubAuthor.index(')')]
							if pubYear != '':
								ttlfile.write(pubID + ' bp3:year "' + pubYear + '"^^xsd:gYear .\n')

				ttlfile.write(currentIdent + ' psimi:sourceDatabase mi:' + row[12][11:15] + ' .\n')
				ttlfile.write('mi:' + row[12][11:15] + ' rdfs:label "' + row[12][17:-1] + '" .\n')
				ttlfile.write(currentIdent + ' bp3:dataSource ' + row[13] + ' .\n')
				ttlfile.write(row[13] + ' rdf:type bp3:Provenance .\n')
				ttlfile.write(row[13] + ' bp3:xref [ rdf:type bp3:UnificationXref ; bp3:db mi:' + row[12][11:15] + ' ; bp3:id "' + row[13][row[13].index(':')+1:] + '" ] .\n')

				lineNumber += 1

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='Convert a MITAB (PSI-MI TAB v2.5) protein interactions file into BioPAX level3.')
	parser.add_argument("mitabFilePath", help="path to the MITAB file")
	parser.add_argument("taxonID", help="identifier for the taxon (Homo sapiens = 996; Saccharomyces cerevisiae S288C = 559292)")
	parser.add_argument("db", help="Source database for the MITAB file [apid|biogrid|dip|intact|unknown]")
	args = parser.parse_args()
	parsePSIMI(args.mitabFilePath, args.taxonID, args.db)

#dataDir = '../../ontology/apid/'
#taxon = 'test'
#dataFileName = taxon + '.mitab'
#parsePSIMI(dataDir + dataFileName, '559292')

## ./mitab2rdf-biopax.py ../../ontology/bioGRID/BIOGRID-ORGANISM-Saccharomyces_cerevisiae_S288c-3.5.188-SUBSET-01.mitab 559292 biogrid
#dataDir = '../../ontology/bioGRID/'
#dataFileName = 'BIOGRID-ORGANISM-Saccharomyces_cerevisiae_S288c-3.5.188-SUBSET-01.mitab'
#parsePSIMI(dataDir + dataFileName, '559292')
