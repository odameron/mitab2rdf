#! /usr/bin/env python3

# MITAB 2.7: https://github.com/HUPO-PSI/miTab/blob/master/PSI-MITAB27Format.md
# PSI-MI: https://www.ebi.ac.uk/ols/ontologies/mi

import argparse
import csv
import pathlib

## TODO: renvoyer un dictionnaire
def parseIdentifierValue(identValue):
	# case psi-mi:"MI:0471"(MINT)
	firstQuoteIndex = identValue.find('"')
	lastQuoteIndex = identValue.rfind('"')
	if (firstQuoteIndex != -1) and (lastQuoteIndex > firstQuoteIndex):
		return identValue[firstQuoteIndex+1:lastQuoteIndex]
	# default
	return identValue


def parseIdentifierValueDict(identValue):
	# expected syntax of identValue:
	# NB: ident can itself contain a colon prefix:qname
	# NB: comment can itsel contain quotes
	# - dbName:ident			e.g. uniprotkb:Q8C052
	# - dbName:"ident"
	# - dbName:ident(comment)
	# - dbName:"ident"(comment)	e.g. psi-mi:"MI:0471"(MINT)
	identDict = {}
	identDict['dbName'] = ''
	identDict['ident'] = identValue
	identDict['comment'] = ''

	indexColonFirst = identValue.find(':')
	if indexColonFirst != -1:
		identDict['dbName'] = identValue[:indexColonFirst]
	else:
		indexColonFirst = 0
	
	if (indexColonFirst < len(identValue) - 1):
		if (identValue[indexColonFirst + 1] == '"'):
			indexQuoteFirst = indexColonFirst + 1
			indexQuoteNext = identValue.find('"', indexQuoteFirst + 1)
			if indexQuoteNext != -1:
				identDict['ident'] = identValue[indexQuoteFirst + 1:indexQuoteNext]
				indexParenOpen = identValue.find('(', indexQuoteNext + 1)
		else:
			indexParenOpen = identValue.find('(', indexColonFirst + 1)
	
		if (indexParenOpen != -1):
			if identDict['ident'] == "":
				identDict['ident'] = identValue[indexColonFirst + 1:indexParenOpen]
			indexParenClose = identValue.rfind(')')
			identDict['comment'] = identValue[indexParenOpen + 1:indexParenClose]
		else:
			if identDict['ident'] == "":
				identDict['ident'] = identValue[indexColonFirst + 1:]
	return identDict



def getInteractorsUniprotID(mitabLine):
	interactorA = mitabLine[0].replace('entrez gene/locuslink:', 'entrezGene:')
	interactorB = mitabLine[1].replace('entrez gene/locuslink:', 'entrezGene:')
	if mitabLine[0].startswith('uniprotkb:'):
		interactorA = mitabLine[0]
	else:
		for altIdent in mitabLine[2].split('|'):
			if altIdent.startswith('uniprotkb:'):
				interactorA = altIdent
				break
			elif altIdent.startswith('uniprot/swiss-prot:'):
				interactorA = 'uniprotkb:' + altIdent[2:]
				break
	if mitabLine[1].startswith('uniprotkb:'):
		interactorB = mitabLine[1]
	else:
		for altIdent in mitabLine[3].split('|'):
			if altIdent.startswith('uniprotkb:'):
				interactorB = altIdent
				break
			elif altIdent.startswith('uniprot/swiss-prot:'):
				interactorB = 'uniprotkb:' + altIdent[3:]
				break
	return (interactorA, interactorB)


def getInteractorLabels(aliasesList):
	labels = []
	for currentAlias in aliasesList:
		if currentAlias.startswith('uniprotkb:'):
			labels.append(currentAlias.replace('uniprotkb:', '').replace('(gene_name)', '').replace('(gene name)', '').replace('(gene name synonym)', '').replace('(locus name)', '').replace('"', ''))
		elif currentAlias.startswith('entrez gene/locuslink:'):
			labels.append(currentAlias.replace('entrez gene/locuslink:', '').replace('(gene name synonym)', '').replace('"', ''))
		elif currentAlias.startswith('psi-mi:'):
			if currentAlias.find('(display_long)') != -1:
				labels.append(currentAlias.replace('psi-mi:', '').replace('(display_long)', '').replace('(gene name)', ''))
			elif currentAlias.find('(display_short)') != -1:
				labels.append(currentAlias.replace('psi-mi:', '').replace('(display_short)', '').replace('(gene name)', ''))
			else:
				labels.append(currentAlias.replace('psi-mi:', '').replace('(gene name)', ''))
	return labels


def getInteractorTaxids(taxidList):
	taxids = []
	taxidLabels = []
	for currentTaxid in taxidList:
		parenthesisIndex = currentTaxid.find('(')
		if parenthesisIndex == -1:
			taxids.append(currentTaxid)
			taxidLabels.append("")
		else:
			taxids.append(currentTaxid[:parenthesisIndex])
			taxidLabels.append(currentTaxid[parenthesisIndex+1:-1])
	return (taxids, taxidLabels)


def parsePSIMI(filepath, taxon, database):
	with open(pathlib.PurePath(filepath).with_suffix('.ttl'), 'w') as ttlfile:
		### TODO: declare prefix imex:
		"""ttlfile.write('PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n')
		ttlfile.write('PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n')
		ttlfile.write('PREFIX owl: <http://www.w3.org/2002/07/owl#>\n')
		ttlfile.write('PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n')
		ttlfile.write('PREFIX skos: <http://www.w3.org/2004/02/skos/core#>\n')
		ttlfile.write('PREFIX dcterms: <http://purl.org/dc/terms/> \n')
		ttlfile.write('\n')
		ttlfile.write('PREFIX psimi: <http://www.psidev.info/>\n')
		ttlfile.write('PREFIX MI: <http://purl.obolibrary.org/obo/MI_>\n')
		ttlfile.write('PREFIX pubmed: <https://www.ncbi.nlm.nih.gov/pubmed/>\n')
		ttlfile.write('PREFIX taxid: <http://purl.uniprot.org/taxonomy/>\n')
		ttlfile.write('PREFIX up: <http://purl.uniprot.org/core/>\n')
		ttlfile.write('PREFIX uniprotkb: <http://purl.uniprot.org/uniprot/>\n')
		ttlfile.write('PREFIX entrezGene: <https://www.ncbi.nlm.nih.gov/gene/>\n')
		ttlfile.write('PREFIX wp: <http://vocabularies.wikipathways.org/wp#>\n')
		ttlfile.write('PREFIX bp3: <http://www.biopax.org/release/biopax-level3.owl#>\n')
		ttlfile.write('\n')
		ttlfile.write('PREFIX apid: <https://apid.dep.usal.es/>\n')
		ttlfile.write('PREFIX biogrid: <http://thebiogrid.org/>\n')
		ttlfile.write('PREFIX dip: <http://dip.doe-mbi.ucla.edu/>\n')
		ttlfile.write('PREFIX intact: <https://www.ebi.ac.uk/intact/interaction/>\n')

		##ttlfile.write('PREFIX : <>.')
		#ttlfile.write('\n')
		#ttlfile.write('apid:interactor1 rdfs:subPropertyOf apid:interactor .\n')
		#ttlfile.write('apid:interactor2 rdfs:subPropertyOf apid:interactor .\n')
		#ttlfile.write('apid:interactor1 rdfs:label "has interactor" .\n')
		#ttlfile.write('apid:interactor2 rdfs:label "has interactor" .\n')
		ttlfile.write('\n') """
		ttlfile.write('\n')


		with open(filepath) as csvfile:
			psimireader = csv.reader(csvfile, delimiter='\t')

			lineNumber = 0
			for row in psimireader:
				#if lineNumber == 0:
				if (len(row) == 0) or (row[0].startswith('#')):
					for i in range(len(row)):
						print(str(i) + '\t' + row[i])
					print('\n')
					lineNumber += 1
					continue
				
				currentIdent = '{}:{}-taxon{}-interaction{}'.format(database, database, taxon, lineNumber)
				print(currentIdent)

				# BioPAX level 3 specifications, page 35
				ttlfile.write(currentIdent + ' rdf:type bp3:MolecularInteraction .\n')

				##### INTERACTORS
				(interactorA, interactorB) = getInteractorsUniprotID(row)
				ttlfile.write(currentIdent + ' bp3:participant ' + interactorA + ' .\n')
				ttlfile.write(currentIdent + ' bp3:participant ' + interactorB + ' .\n')

				ttlfile.write(interactorA + ' rdf:type up:Protein .\n')
				for interactorLabel in getInteractorLabels(row[4].split('|')):
					ttlfile.write(interactorA + ' rdfs:label "' + interactorLabel + '" .\n')

				ttlfile.write(interactorB + ' rdf:type up:Protein .\n')
				for interactorLabel in getInteractorLabels(row[5].split('|')):
					ttlfile.write(interactorB + ' rdfs:label "' + interactorLabel + '" .\n')

				(taxids, taxidLabels) = getInteractorTaxids(row[9].split('|'))
				for i in range(len(taxids)):
					interactorTaxid = taxids[i]
					interactorTaxidLabel = taxidLabels[i]
					ttlfile.write(interactorA + ' up:organism ' + taxids[i] + ' .\n')
					if taxidLabels[i] != "":
						ttlfile.write(taxids[i] + ' rdfs:label "' + taxidLabels[i] + '" .\n')
				(taxids, taxidLabels) = getInteractorTaxids(row[10].split('|'))
				for i in range(len(taxids)):
					interactorTaxid = taxids[i]
					interactorTaxidLabel = taxidLabels[i]
					ttlfile.write(interactorB + ' up:organism ' + taxids[i] + ' .\n')
					if taxidLabels[i] != "":
						ttlfile.write(taxids[i] + ' rdfs:label "' + taxidLabels[i] + '" .\n')
				

				##### FIELD 6: interaction detection method
				### TODO: replace psimi:detectionMethod by a (PSI-MI?) URI
				indexInteractionDetectionMethod = 6
				#ttlfile.write(currentIdent + ' psimi:detectionMethod MI:' + row[indexInteractionDetectionMethod][11:15] + ' .\n')
				ttlfile.write(currentIdent + ' psimi:detectionMethod ' + parseIdentifierValueDict(row[indexInteractionDetectionMethod])['ident'] + ' .\n')


				##### FIELDS 7-8: publication
				### TODO: for fields, replace indexXYZ by indexFieldXYZ
				### TODO: replace psimi:publicationIdentifier by a (PSI-MI?) URI
				indexPublicationAuthor = 7
				indexPublicationIdent = 8
				listPubID = row[indexPublicationIdent].split('|')
				listPubAuth = row[indexPublicationAuthor].split('|')
				for pubIDindex in range(len(listPubID)):
					pubID = listPubID[pubIDindex]
					ttlfile.write(currentIdent + ' psimi:publicationIdentifier ' + pubID + ' .\n')
					ttlfile.write(currentIdent + ' bp3:xref ' + pubID + ' .\n')
					#ttlfile.write(pubID + ' rdf:type wp:PublicationReference .\n')
					ttlfile.write(pubID + ' rdf:type bp3:PublicationXref .\n')
					if pubID.startswith('pubmed:'):
						ttlfile.write(pubID + ' bp3:db mi:0446 .\n')
						ttlfile.write('mi:0445 rdfs:label "PubMed" .\n')
						ttlfile.write(pubID + ' bp3:id "' + pubID.replace('pubmed:', '') + '" .\n')
					elif pubID.startswith('imex:'):
						ttlfile.write(pubID + ' bp3:db mi:0670 .\n')
						ttlfile.write('mi:0445 rdfs:label "imex" .\n')
						ttlfile.write(pubID + ' bp3:id "' + pubID.replace('imex:', '') + '" .\n')
					if pubIDindex < len(listPubAuth):
						ttlfile.write(pubID + ' psimi:publication1stAuthor "' + listPubAuth[pubIDindex] + '" .\n')
						pubAuthor = listPubAuth[pubIDindex]
						etAlIndex = pubAuthor.find(' et al.')
						if etAlIndex == -1:
							pubAuthorName = pubAuthor
						else:
							pubAuthorName = pubAuthor[:etAlIndex]
						if pubAuthorName != '':
							ttlfile.write(pubID + ' bp3:author "' + pubAuthorName + '" .\n')
						yearIndex = pubAuthor.find('(')
						if yearIndex != -1:
							pubYear = pubAuthor[pubAuthor.index('(')+1:pubAuthor.index(')')]
							if pubYear != '':
								ttlfile.write(pubID + ' bp3:year "' + pubYear + '"^^xsd:gYear .\n')

				ttlfile.write(currentIdent + ' psimi:sourceDatabase mi:' + row[12][11:15] + ' .\n')
				ttlfile.write('mi:' + row[12][11:15] + ' rdfs:label "' + row[12][17:-1] + '" .\n')
				ttlfile.write(currentIdent + ' bp3:dataSource ' + row[13] + ' .\n')
				ttlfile.write(row[13] + ' rdf:type bp3:Provenance .\n')
				ttlfile.write(row[13] + ' bp3:xref [ rdf:type bp3:UnificationXref ; bp3:db mi:' + row[12][11:15] + ' ; bp3:id "' + row[13][row[13].index(':')+1:] + '" ] .\n')
				

				##### FILED 11: interaction type(s)
				### TODO: replace psimi:interactionType by a (PSI-MI?) URI 
				indexFieldInteractionType = 11
				if row[indexFieldInteractionType] != "" and row[indexFieldInteractionType] != "-":
					for currentInteraction in row[indexFieldInteractionType].split('|'):
						currentInteractionDict = parseIdentifierValueDict(currentInteraction)
						ttlfile.write(currentIdent + ' psimi:interactionType ' + currentInteractionDict['ident'] + ' .\n')
						if currentInteractionDict['comment'] != "":
							ttlfile.write(currentInteractionDict['ident'] + ' rdfs:label "' + currentInteractionDict['comment'] + '" .\n')


				##### FIELD 12: source database
				indexSourceDatabase = 12
				#print("??? >" + row[indexSourceDatabase] + "<")
				if (row[indexSourceDatabase] != '') and (row[indexSourceDatabase] != '-'):
					ttlfile.write('{} bp3:dataSource {} .\n'.format(currentIdent, parseIdentifierValue(row[indexSourceDatabase])))

				
				##### FIELD 13: interaction identifier(s)
				### TODO: use bp3:xref in addition to owl:sameAs
				indexFieldInteractionIdentifier = 13
				if row[indexFieldInteractionIdentifier] != "" and row[indexFieldInteractionIdentifier] != "-":
					for currentInteraction in row[indexFieldInteractionIdentifier].split('|'):
						currentInteractionDict = parseIdentifierValueDict(currentInteraction)
						### TODO: BUG currentInteractionDict['ident'] is intact:EBI-27034374 ans should be EBI-27034374
						ttlfile.write(currentIdent + ' owl:sameAs ' + currentInteractionDict['ident'] + ' .\n')
						print("????? " + currentInteractionDict['dbName'])

				lineNumber += 1


				##### FILED 14: confidence value(s)






if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='Convert a MITAB (PSI-MI TAB v2.7) protein interactions file into BioPAX level3.')
	parser.add_argument("mitabFilePath", help="path to the MITAB file")
	### TODO: retrieve taxon by concatenating columns Taxid interactor A and Taxid interactor B
	parser.add_argument("taxonID", help="identifier for the taxon (Homo sapiens = 996; Saccharomyces cerevisiae S288C = 559292)")
	parser.add_argument("db", help="Source database for the MITAB file [apid|biogrid|dip|intact|unknown]")
	args = parser.parse_args()
	parsePSIMI(args.mitabFilePath, args.taxonID, args.db)

#
# ./mitab2rdf-biopax-mitab27.py ../../ontology/intact-biopax/test1.mitab27 9606 intact
#
# ./mitab2rdf-biopax-mitab27.py ../../ontology/intact-biopax/test1.mitab27 9606 intact && cat ../../ontology/intact-biopax/test1.ttl
#
#dataDir = '../../ontology/intact-biopax/'
#dataFileName = 'test1.mitab27'
#parsePSIMI(dataDir + dataFileName)
